(function ($) {
    $.fn.addSpinner = function () {
        this.prop('disabled', true);
        this.find('i').addClass('hide');
        return this.prepend('<i class="fa fa-spinner fa-spin"></i> ');
    };

    $.fn.removeSpinner = function () {
        this.prop("disabled", false);
        this.find('i.fa-spinner').remove();
        this.find('i.hide').removeClass('hide');
    };
}(jQuery));

$(document).ready(function () {
    
    $callBackModel = $("#call_back");
    $callBackForm = $callBackModel.find('.call_back_form'); 
    $callBackForm.off().on('submit', function () {
        var $btn = $callBackForm.find('.btn-submit');
        $btn.addSpinner();
        $.ajax({
            url: $(this).data("url"),
            type: 'POST',
            dataType: 'json',
            data: $(this).serialize(),
            context: this,
            complete: function(){
                $btn.removeSpinner();
            },
            success: function (result) {
                if (result.status === 'success') {
                    $callBackForm.hide();
                    $callBackModel.find('.thank-you-pop').fadeIn();
                } else {
                }
            }
        });
        return false;
    });

    $callBackModel.on("hidden.bs.modal", function () {
        $callBackForm.show();
        $callBackForm.find('input').val("");
        $callBackModel.find('.thank-you-pop').hide();
    });
});