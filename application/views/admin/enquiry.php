<div class="dashboard-content">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-xs-12">
            <div class="dashboard-list-box">
                <h4 class="gray">All Enquiry</h4>
                <div class="table-box">
                    <table class="basic-table booking-table">
                        <thead>
                            <tr>
                                <th>Date</th>
                                <th>Name</th>
                                <th>Destination</th>
                                <th>Phone</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                             <?php 
                            foreach($enquiry as $row){
                                echo "<tr>";
                                $date = date_create($row['created_at']);
                                echo "<td>".  date_format($date, "d/m/Y")."</td>";
                                echo "<td>".$row['name']."</td>";
                                echo "<td>".$row['destination']."</td>";
                                echo "<td>".$row['mobile']."</td>";
                                echo '<td><a href="#" class="button gray"><i class="sl sl-icon-pencil"></i></a>'.
                                    '<a href="#" class="button gray"><i class="sl sl-icon-close"></i></a>'.
                                    '</td>';
                                echo "</tr>";
                            }
                            
                            ?>
                            
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="pagination-container">
                <nav class="pagination">
                    <ul>
                        <li><a href="#" class="current-page">1</a></li>
                        <li><a href="#">2</a></li>
                        <li><a href="#"><i class="sl sl-icon-arrow-right"></i></a></li>
                    </ul>
                </nav>
            </div>
        </div>
    </div>
</div>