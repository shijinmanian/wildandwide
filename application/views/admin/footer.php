</div>
<!-- Dashboard / End -->

</div>
<!-- end Container Wrapper -->

<!-- Back to top start -->
<div id="back-to-top">
    <a href="#"></a>
</div>
<!-- Back to top ends -->

<!-- *Scripts* -->
<script src="../../../../canvasjs.com/assets/script/canvasjs.min.js"></script>

<!-- *Scripts* -->
<script src="<?php echo base_url('assets/js/jquery-3.2.1.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/bootstrap.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/plugin.js') ?>"></script>
<script src="<?php echo base_url('assets/js/main.js') ?>"></script>
<script src="<?php echo base_url('assets/') ?>js/custom-countdown.js"></script>
<script src="<?php echo base_url('assets/js/preloader.js') ?>"></script>
<script src="<?php echo base_url('assets/js/jpanelmenu.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/chart.js') ?>"></script>
<script src="<?php echo base_url('assets/js/counterup.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/dashboard-custom.js') ?>"></script>
