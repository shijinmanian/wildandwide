<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" lang="zxx">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title> WILD & WIDE - Photo Tour</title>
    <!-- Favicon -->
    <link rel="shortcut icon" type="image/x-icon" href="<?php echo base_url('assets/')?>images/logo1.png">
    <!-- Bootstrap core CSS -->
    <link href="<?php echo base_url('assets/css/bootstrap.min.css'); ?>" rel="stylesheet" type="text/css">
    <!--Custom CSS-->
    <link href="<?php echo base_url('assets/css/style.css'); ?>" rel="stylesheet" type="text/css">
    <!--Flaticons CSS-->
    <link href="<?php echo base_url('assets/font/flaticon.css'); ?>" rel="stylesheet" type="text/css">
    <!--Plugin CSS-->
    <link href="<?php echo base_url('assets/css/plugin.css'); ?>" rel="stylesheet" type="text/css">
    <!--Font Awesome-->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
</head>
<body>
    
<!-- Preloader -->
<div id="preloader hide" >
   <div id="status"></div>
</div>
<!-- Preloader Ends -->

 <!-- Header -->
    <header>
        <div class="upper-head clearfix">
            <div class="container">
                <div class="contact-info">
                    <p><i class="flaticon-phone-call"></i> Phone: (+91) 9207362428</p>
                    <p><i class="flaticon-mail"></i> Mail: wandwpt@gmail.com</p>
                </div>
                <div class="login-btn pull-right">
                    <a href="login.html"><i class="fa fa-user-plus"></i> Register</a>
                    <a href="<?php echo base_url('auth/login')?>"><i class="fa fa-unlock-alt"></i> Login</a>
                </div>
            </div>
        </div>
    </header>
    <!-- Header Ends -->
    
    
    <!-- Navigation Bar -->
    <div class="navigation">  
        <div class="container">
            <div class="navigation-content">
                <div class="header_menu">
                    <div class="row">
                        <div class="col-md-10">
                            <!-- start Navbar (Header) -->
                            <nav class="navbar navbar-default navbar-sticky-function navbar-arrow">
                                <div class="logo pull-left">
                                    <a href="<?php echo base_url() ?>"><img alt="Image" src="<?php echo base_url('assets/')?>images/logo.jpg"></a>
                                </div>
                                <div id="navbar" class="navbar-nav-wrapper">
                                    <ul class="nav navbar-nav" id="responsive-menu">
                                        <li class="active">
                                            <a href="<?php echo base_url() ?>">Home</i></a>
                                         <li>
                                            <a href="#">Photography <i class="fa fa-angle-down"></i></a>
                                            <ul>
                                                <li> <a href="#">Photo Tour's</a> </li>
                                                <li> <a href="#">Road Trip</a> </li>

                                            </ul>
                                        </li>
                                        <li >
                                            <a href="">Packages <i class="fa fa-angle-down"></i></a>
                                            <ul>
                                                <li> <a href="">Indian tour packages</a></li>
                                                <li> <a href="">International tours (100 customizable trips)</a></li>
                                                <li> <a href="">Special packages (holy land etc)</a></li>
                                            </ul>
                                        </li>
                                        <li><a href="">Gallery</a></li>
                                        <li><a href="">History</a></li>
                                        <li><a href="">Contact us</a></li>
                                        <li><a href="">Enquiry</a></li> 
                                        <li><a href="">Other </a></li>
                                    </ul>
                                </div><!--/.nav-collapse -->
                                <div id="slicknav-mobile"></div>
                            </nav>
                        </div>
                        <div class="col-md-2">
                            <a class="btn-red call-btn" data-toggle="modal" data-target="#call_back">Get a call</a>
                            
                            <div id="call_back" class="modal fade" role="dialog" aria-hidden="true" data-backdrop="static">
                                <div class="modal-dialog">
                                  <!-- Modal content-->
                                  <div class="modal-content">
                                        <div class="modal-header">
                                          <button type="button" class="close" data-dismiss="modal">&times;</button>
                                          <h4 class="modal-title">Send us a query</h4>
                                        </div>
                                        <div class="modal-body">
                                            <form class="call_back_form" action="javascript:void(0);" data-url="<?php echo base_url("enquiry/add") ?>">
                                                <div class="form-group">
                                                    <label for="name">Name</label>
                                                    <input type="text" name="name" class="form-control" id="name">
                                                </div>
                                                <div class="form-group">
                                                    <label for="email">Email ID</label>
                                                    <input type="email" name="email" class="form-control" id="email">
                                                </div>
                                                <div class="form-group">
                                                    <label for="name">Phone</label>
                                                    <input type="text" name="mobile" class="form-control ">
                                                </div>
                                                <div class="form-group">
                                                    <label for="email">Destination</label>
                                                    <input type="hidden" name="destination_lat" class="destination_lat">
                                                    <input type="hidden" name="destination_lng" class="destination_lng">
                                                    <input type="text" name="destination" class="form-control" id="google_map">
                                                </div>

                                                <div class="modal-footer-inner">  
                                                    <button type="submit" class="btn-submit btn btn-danger">GET A CALL BACK</button>
                                                </div>
                                            </form>
                                            
                                            
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="thank-you-pop text-center" style="display: none">
                                                        <div class="icon text-success  big-icon"><i class="fa fa-check-circle-o"></i> </div>
                                                        <h1 class="text-center">Thank You!</h1>
                                                        <p>Your enquiry submission is received and we will contact you soon</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <div class="modal-footer">
                                            
                                        </div>
                                    
                                    
                                    
                                  </div>

                                </div>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Navigation Bar Ends -->