<section class="login">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-md-offset-3">
                <div class="login-form">
                    <?php echo form_open("auth/login"); ?>
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="form-title">
                                <h2><?php echo lang('login_heading');?></h2>
                                <!--<p>Register if you don't have an account.</p>-->
                                <p><?php echo lang('login_subheading');?></p
                            </div>
                        </div>

                        <?php if(isset($message)&& $message){ ?>
                            <div class="alert alert-danger" role="alert"><?php echo $message; ?></div>
                        <?php } ?>

                        <div class="form-group">
                            <label ><?php echo lang('login_identity_label', 'identity'); ?></label>
                            <!--<input type="email" class="form-control" id="Name1" placeholder="Enter username or email id">-->
                                <?php echo form_input($identity, '', 'class="form-control" type="email" placeholder="Enter username or email id"'); ?>

                        </div>
                        <div class="form-group ">
                            <label>Password</label>
                            <!--<input type="password" class="form-control" id="email1" >-->
                            <?php echo form_input($password, '', 'class="form-control" placeholder="Enter correct password"'); ?>

                        </div>
                        <div class="col-xs-12">
                            <div class="checkbox-outer">
                                <!--<input type="checkbox" name="vehicle2" value="Car">--> 
                                    <?php echo form_checkbox('remember', '1', FALSE, 'id="remember"'); ?>Remember Me?

                            </div>
                        </div>
                        <div class="col-xs-12">
                            <div class="comment-btn">
                                <p><?php echo form_submit('submit', lang('login_submit_btn'), 'class="btn btn-danger btn-blue btn-red"'); ?></p>

                                <!--<a href="#" class="btn-blue btn-red">Login</a>-->
                            </div>
                        </div>
                        <div class="col-xs-12">
                            <div class="login-accounts">
                                <a href="forgot_password" class="forgotpw">Forgot Password?</a>
                                <!--                                <h3>Login using</h3>
                                                                <div class="login-accounts-btn">
                                                                    <a class="btn-blue" href="#"><i class="fa fa-facebook" aria-hidden="true"></i> Facebook</a>
                                                                    <a class="btn-blue btn-google" href="#"><i class="fa fa-google" aria-hidden="true"></i> Google</a>                              
                                                                    <a class="btn-blue btn-twit" href="#"><i class="fa fa-twitter" aria-hidden="true"></i> Twitter</a>
                                                                </div>-->
                            </div>
                        </div>
                    </div>
                    <?php echo form_close(); ?>
                </div>
            </div>
<!--            <div class="col-md-6">
                <div class="login-form">
                    <form>
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="form-title">
                                    <h2>Register</h2>
                                    <p>Enter your details to be a member.</p>
                                </div>
                            </div>
                            <div class="form-group col-xs-12">
                                <label>Name:</label>
                                <input type="text" class="form-control" id="Name" placeholder="Enter full name">
                            </div>
                            <div class="form-group col-xs-12">
                                <label>Email:</label>
                                <input type="email" class="form-control" id="email" placeholder="abc@xyz.com">
                            </div>
                            <div class="form-group col-xs-12">
                                <label>Phone Number:</label>
                                <input type="text" class="form-control" id="date1" placeholder="Select Date">
                            </div>
                            <div class="form-group col-xs-6">
                                <label>Select Password :</label>
                                <input type="password" class="form-control" id="date" placeholder="Enter Password">
                            </div>
                            <div class="form-group col-xs-6 col-left-padding">
                                <label>Confirm Password :</label>
                                <input type="password" class="form-control" id="phnumber" placeholder="Re-enter Password">
                            </div>
                            <div class="col-xs-12">
                                <div class="checkbox-outer">
                                    <input type="checkbox" name="vehicle2" value="Car"> I agree to the <a href="#">terms and conditions.</a>
                                </div>
                            </div>
                            <div class="col-xs-12">
                                <div class="comment-btn">
                                    <a href="#" class="btn-blue btn-red">Register Now</a>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>-->
        </div>
    </div>
</section>


 
