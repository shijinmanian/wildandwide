<!-- *Scripts* -->
<script src="<?php echo base_url('assets/js/jquery-3.2.1.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/bootstrap.min.js') ?>"></script>
<script src="<?php echo base_url('assets/js/plugin.js') ?>"></script>
<script src="<?php echo base_url('assets/js/main.js') ?>"></script>
<script src="<?php echo base_url('assets/js/main-1.js') ?>"></script>
<!--<script src="<?php // echo base_url('assets/') ?>js/custom-countdown.js"></script>-->
<script src="<?php echo base_url('assets/js/preloader.js') ?>"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBksI4mQQ8CS_TX9DmyptCJuFd-vI_9xlY&libraries=places&callback=initMap"  async defer></script>
<script src="<?php echo base_url('assets/js/home.js') ?>"></script>

<script>     
      function initMap() {
        var input = document.getElementById('google_map');
        var autocomplete = new google.maps.places.Autocomplete(input);
        autocomplete.addListener('place_changed', function() {
            var place = autocomplete.getPlace();
            if (!place.geometry) {
              // User entered the name of a Place that was not suggested and
              // pressed the Enter key, or the Place Details request failed.
              window.alert("No details available for input: '" + place.name + "'");
              return;
            }

            var address = '';
            if (place.address_components) {
              address = [
                (place.address_components[0] && place.address_components[0].short_name || ''),
                (place.address_components[1] && place.address_components[1].short_name || ''),
                (place.address_components[2] && place.address_components[2].short_name || '')
              ].join(' ');
            }
            
            $(".destination_lat").val(place.geometry.location.lat());
            $(".destination_lng").val(place.geometry.location.lng());
        });
        
    };
//         google.maps.event.addDomListener(window, 'load', initMap);
    </script>