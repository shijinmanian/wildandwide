    <!-- Banner -->
    <section id="home_banner">
        <!-- Paradise Slider -->
        <div id="kenburns_061" class="carousel slide ps_indicators_txt_icon ps_control_txt_icon kbrns_zoomInOut thumb_scroll_x swipe_x ps_easeOutQuart" data-ride="carousel" data-pause="hover" data-interval="10000" data-duration="2000">
            <!-- Wrapper For Slides -->
            <div class="carousel-inner" role="listbox">

                <!-- First Slide -->
                <div class="item active">
                    <!-- Slide Background -->
                    <img src="<?php echo base_url('assets/')?>images/slider1.jpg" alt="kenburns_061_01" />
                    <!-- Left Slide Text Layer -->
                    <div class="kenburns_061_slide" data-animation="animated fadeInRight">
                        <h2>Welcome To Wild & Wide Tours</h2>
                        <h1>Dream your Wonderful Photo Tours</h1>
                        <a href="tour-detail.html" class="btn-red btn-red">Explore wide</a>
                    </div><!-- /Left Slide Text Layer -->
                </div><!-- /item -->
                <!-- End of Slide -->

                <!-- Second Slide -->
                <div class="item">
                    <!-- Slide Background -->
                    <img src="<?php echo base_url('assets/')?>images/slider2.jpg" alt="kenburns_061_02" />
                    <!-- Right Slide Text Layer -->
                    <div class="kenburns_061_slide" data-animation="animated fadeInUp">
                        <h2>exciting schemes just a click away</h2>
                        <h1>Quality Holidays With Us</h1>
                        <a href="tour-detail.html" class="btn-red btn-red">View More</a>
                    </div><!-- /Right Slide Text Layer -->
                </div><!-- /item -->

                <!-- Third Slide -->
                <div class="item">
                    <!-- Slide Background -->
                    <img src="<?php echo base_url('assets/')?>images/slider3.jpg" alt="kenburns_061_02" />
                    <!-- Right Slide Text Layer -->
                    <div class="kenburns_061_slide" data-animation="animated fadeInDown">
                        <h2>Cost friendly packages on your way</h2>
                        <h1>Everything is here right For u</h1>
                        <a href="tour-detail.html" class="btn-red btn-red">Book Now</a>
                    </div><!-- /Right Slide Text Layer -->
                </div><!-- /item -->
                <!-- End of Slide -->

            </div><!-- End of Wrapper For Slides -->

            <!-- Left Control -->
            <a class="left carousel-control" href="#kenburns_061" role="button" data-slide="prev">
                <span>prev</span>
                <span class="sr-only">Previous</span>
            </a>

            <!-- Right Control -->
            <a class="right carousel-control" href="#kenburns_061" role="button" data-slide="next">
                <span>next</span>
                <span class="sr-only">Next</span>
            </a>
        </div> <!-- End Paradise Slider -->

        <!-- Search Box -->
<!--        <div class="search-box clearfix">
            
            <div class="search-outer">
                <h3 class="text-center">Quick Booking</h3>
                <div class="search-content table_item">
                    
                    <div id="map" ></div>
                    <form>
                        <div class="form-group">
                            <div class='input-group'>
                                <input type='text' class="form-control" id="google_map" value="" placeholder="Where to"/>
                                <i class="flaticon-calendar"></i>
                                <span class="input-group-addon">
                                    <i class="fa fa-map-marker" aria-hidden="true"></i>
                                </span>
                            </div>
                        </div>

                        <div class="form-group form-icon">
                            <div class='input-group date' id='datetimepicker2'>
                                <input type='text' class="form-control" value="Check Out" />
                                <i class="flaticon-calendar"></i>
                                <span class="input-group-addon">
                                    <i class="fa fa-calendar" aria-hidden="true"></i>
                                </span>
                            </div>
                        </div>

                        <div class="form-group  form-icon">
                            <select name="custom-select-2" class="selectpicker form-control" tabindex="1">
                                <option value="0">Tour Type</option>
                                <option value="1">0</option>
                                <option value="2">1</option>
                                <option value="3">2</option>
                                <option value="4">3</option>
                                <option value="5">4</option>
                            </select>
                            <i class="flaticon-box"></i>
                        </div>

                         

                        <div class="search">
                            <a href="#" class="btn-red btn-red">CHECK AVAILABILITY</a>
                        </div>        
                    </form> 
                </div>
            </div>
        </div>-->
        <!-- Search Box Ends -->

    </section>
    <!-- Banner Ends -->

    <section id="mt_about">
        <div class="container">

            <div class="row">
                
                <div class="col-md-6 col-sm-12">
                    <!-- <div class="square-bg-block"></div> -->
                    <div class="image-rev">
                        <div class="blur-img" style="background-image: url(<?php echo base_url('assets/')?>images/list8.jpg);"></div>
                        <img src="<?php echo base_url('assets/')?>images/list8.jpg" alt="">
                    </div>
                </div>

                <div class="col-md-6 col-sm-12">
                    <div class="about_services text-center">
                        <h4>About Us</h4>
                        <h2 class="text-uppercase">Here is a tribute to <span>good Tour!</span></h2>
                        <p> Explore the Wild and Wide  nature with us, We want to help people to travel as much as they can at the best price and with a wide variety of travel resources. Both passionate and professional Photographers can also accompany with us to explore a lot.</p>
                        <a href="about-us.html" class="btn-red">Learn More</a>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- Popular Packages --> 
    <section class="popular-packages">
        <div class="container">
            <div class="section-title">
                <h2>Prominent <span>Destinations</span></h2>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt .</p>
            </div>
            <div class="row room-slider slider-button">
                <div class="col-sm-4">
                    <div class="package-item">
                        <img src="<?php echo base_url('assets/')?>images/hotel/room-1.jpg" alt="Image">
                        <div class="package-content">
                            <h5>Starting: <span>Rs 659</span> / Per day </h5>
                            <h3><a href="">Munnar</a></h3>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod.</p>
                        </div>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="package-item">
                        <img src="<?php echo base_url('assets/')?>images/hotel/room-2.jpg" alt="Image">
                        <div class="package-content">
                            <h5>Starting: <span>Rs 459</span> / Per day </h5>
                            <h3><a href="">Munnar</a></h3>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod.</p> 
                        </div>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="package-item">
                        <img src="<?php echo base_url('assets/')?>images/hotel/room-3.jpg" alt="Image">
                        <div class="package-content">
                            <h5>Starting: <span>Rs 259</span> / Per day </h5>
                            <h3><a href="">Munnar</a></h3>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod.</p>
                        </div>
                    </div>
                </div>

                <div class="col-sm-4">
                    <div class="package-item">
                        <img src="<?php echo base_url('assets/')?>images/hotel/room-4.jpg" alt="Image">
                        <div class="package-content">
                            <h5>Starting: <span>Rs 159</span> / Per day </h5>
                            <h3><a href="hotel-detail.html">Single Room</a></h3>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Popular Packages Ends -->


    <!-- Trip Ad -->
    <section class="cta">
        <div class="container">
            <div class="cta-content text-center">
                <div class="cta-title">
                    <h3 class="white">Coming Soon</h3>
                    <h2 class="white text-uppercase">Enjoy the Road Trip</h2>
                    <h3 class="white">Kaniyakumari To Kashmir </h3>
                </div>
                <div class="cta-btn">
                    <a href="hotel-detail.html" class="btn-red btn-red">BOOK NOW</a>
                </div>
            </div>
        </div>
    </section>
    <!-- Trip Ad Ends -->

    <!-- Deals On Sale -->
    <section class="deals-on-sale">
        <div class="container">
            <div class="section-title">
                <h2>Amazing Places</h2>
                <p>THE BEST VALUE UNDER THE SUN</p>
            </div>
            <div class="row sale-slider slider-button">
                <div class="col-md-12">
                    <div class="sale-item">
                        <div class="sale-image">
                            <img src="<?php echo base_url('assets/')?>images/sale1.jpg" alt="Image">
                        </div>
                        <div class="sale-content">
                            <div class="deal-rating">
                                <span class="fa fa-star checked"></span>
                                <span class="fa fa-star checked"></span>
                                <span class="fa fa-star checked"></span>
                                <span class="fa fa-star checked"></span>
                                <span class="fa fa-star checked"></span>
                            </div>
                            <h3><a href="hotel-detail.html" class="white">Surfing Bahamas</a></h3>
                        </div>  
                        <div class="sale-overlay"></div>  
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="sale-item">
                        <div class="sale-image">
                            <img src="<?php echo base_url('assets/')?>images/sale2.jpg" alt="Image">
                        </div>
                        <div class="sale-content">
                            <div class="deal-rating">
                                <span class="fa fa-star checked"></span>
                                <span class="fa fa-star checked"></span>
                                <span class="fa fa-star checked"></span>
                                <span class="fa fa-star checked"></span>
                                <span class="fa fa-star checked"></span>
                            </div>
                            <h3><a href="hotel-detail.html" class="white">Mountain City</a></h3>
                        </div>  
                        <div class="sale-overlay"></div>  
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="sale-item">
                        <div class="sale-image">
                            <img src="<?php echo base_url('assets/')?>images/sale3.jpg" alt="Image">
                        </div>
                        <div class="sale-content">
                            <div class="deal-rating">
                                <span class="fa fa-star checked"></span>
                                <span class="fa fa-star checked"></span>
                                <span class="fa fa-star checked"></span>
                                <span class="fa fa-star checked"></span>
                                <span class="fa fa-star checked"></span>
                            </div>
                            <h3><a href="hotel-detail.html" class="white">Seneora Beach</a></h3>
                        </div>
                        <div class="sale-overlay"></div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="sale-item">
                        <div class="sale-image">
                            <img src="<?php echo base_url('assets/')?>images/sale4.jpg" alt="Image">
                        </div>
                        <div class="sale-content">
                            <div class="deal-rating">
                                <span class="fa fa-star checked"></span>
                                <span class="fa fa-star checked"></span>
                                <span class="fa fa-star checked"></span>
                                <span class="fa fa-star checked"></span>
                                <span class="fa fa-star checked"></span>
                            </div>
                            <h3><a href="hotel-detail.html" class="white">Beach Market</a></h3>
                        </div>
                        <div class="sale-overlay"></div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Deals On Sale Ends -->


    <!-- Testimonials -->
    <section class="testimonials">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="testimonial-inner">
                        <div class="testimonial-title text-center mar-bottom-35">
                            <h3>CUSTOMER <span>REVIEWS</span></h3>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt</p>
                        </div>
                        <!-- Paradise Slider -->
                        <div id="testimonial_095" class="carousel slide testimonial_095_indicators testimonial_095_control_button thumb_scroll_x swipe_x ps_easeOutSine" data-ride="carousel">
                            <!-- Wrapper For Slides -->
                            <div class="carousel-inner" role="listbox">

                                <!-- First Slide -->
                                <div class="item active">

                                    <!-- Text Layer -->
                                    <div class="testimonial_095_slide">
                                        <div class="testimonial-image">
                                            <img src="<?php echo base_url('assets/')?>images/testemonial2.jpg" alt="Image">
                                        </div>
                                         <div class="testi-heading text-center">
                                            <h4 class="mar-top-10"><a href="#">Susan Doe, Houston</a></h4>
                                            <h5><a href="#">Adventurer</a></h5>
                                        </div>
                                        <p>Lorem ipsum dolor sit amet consectetuer adipiscing elit am nibh unc varius facilisis eros ed erat in in velit quis arcu ornare laoreet urabitur adipiscing luctus massa nteger ut purus ac augue commodo commodo unc nec mi eu justo tempor consectetuer tiam.</p>
                                        <div class="deal-rating">
                                            <span class="fa fa-star checked"></span>
                                            <span class="fa fa-star checked"></span>
                                            <span class="fa fa-star checked"></span>
                                            <span class="fa fa-star-o"></span>
                                            <span class="fa fa-star-o"></span>
                                        </div>
                                    </div> <!-- /Text Layer -->
                                </div> <!-- /item -->
                                <!-- End of First Slide -->

                                <!-- Second Slide -->
                                <div class="item">
                                    <!-- Text Layer -->
                                    <div class="testimonial_095_slide">
                                        <div class="testimonial-image">
                                            <img src="<?php echo base_url('assets/')?>images/testemonial2.jpg" alt="Image">
                                        </div>
                                         <div class="testi-heading text-center">
                                            <h4 class="mar-top-10"><a href="#">Susan Doe, Houston</a></h4>
                                            <h5><a href="#">Adventurer</a></h5>
                                        </div>
                                        <p>Lorem ipsum dolor sit amet consectetuer adipiscing elit am nibh unc varius facilisis eros ed erat in in velit quis arcu ornare laoreet urabitur adipiscing luctus massa nteger ut purus ac augue commodo commodo unc nec mi eu justo tempor consectetuer tiam.</p>
                                        <div class="deal-rating">
                                            <span class="fa fa-star checked"></span>
                                            <span class="fa fa-star checked"></span>
                                            <span class="fa fa-star checked"></span>
                                            <span class="fa fa-star-o"></span>
                                            <span class="fa fa-star-o"></span>
                                        </div>
                                    </div> <!-- /Text Layer -->
                                </div> <!-- /item -->
                                <!-- End of Second Slide -->

                                <!-- Third Slide -->
                                <div class="item">
                                    <!-- Text Layer -->
                                    <div class="testimonial_095_slide">
                                        <div class="testimonial-image">
                                            <img src="<?php echo base_url('assets/')?>images/testemonial2.jpg" alt="Image">
                                        </div>
                                         <div class="testi-heading text-center">
                                            <h4 class="mar-top-10"><a href="#">Susan Doe, Houston</a></h4>
                                            <h5><a href="#">Adventurer</a></h5>
                                        </div>
                                        <p>Lorem ipsum dolor sit amet consectetuer adipiscing elit am nibh unc varius facilisis eros ed erat in in velit quis arcu ornare laoreet urabitur adipiscing luctus massa nteger ut purus ac augue commodo commodo unc nec mi eu justo tempor consectetuer tiam.</p>
                                        <div class="deal-rating">
                                            <span class="fa fa-star checked"></span>
                                            <span class="fa fa-star checked"></span>
                                            <span class="fa fa-star checked"></span>
                                            <span class="fa fa-star-o"></span>
                                            <span class="fa fa-star-o"></span>
                                        </div>
                                    </div> <!-- /Text Layer -->
                                </div> <!-- /item -->
                                <!-- End of Third Slide -->

                            </div> <!-- End of Wrapper For Slides -->

                            <!-- Left Control -->
                            <a class="left carousel-control" href="#testimonial_095" role="button" data-slide="prev">
                                <span class="fa fa-chevron-left"></span>
                            </a>

                            <!-- Right Control -->
                            <a class="right carousel-control" href="#testimonial_095" role="button" data-slide="next">
                                <span class="fa fa-chevron-right"></span>
                            </a>

                        </div> <!-- End Paradise Slider -->
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Testimonials Ends -->

<!--     Countdown 
    <section class="countdown-section">
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                    <div class="countdown-title">
                        <h3 class="white">Hot offer</h3>
                        <h2 class="white">GET <span>40% DISCOUNT</span> ONLY IN SUMMER VOCATIONS</h2>
                        <a href="#" class="btn-red mar-top-15">Book Now</a>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="countdown countdown-container">
                        <h3 class="white">Limited offer</h3>
                        <p id="demo"></p>
                    </div> /.countdown-wrapper 
                </div>
            </div>
        </div>
    </section>
     Countdown Ends 


    -->

    <!-- Footer -->
    <footer>
        <div class="footer-upper">
            <div class="container">

                <div class="footer-links">
                    <div class="row">
                        <div class="col-md-4 col-sm-12">
                            <div class="footer-about footer-margin">
                                <div class="about-logo">
                                    <img src="<?php echo base_url('assets/')?>images/logo.jz" alt="Image">
                                </div>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt.</p>
                                <div class="about-location">
                                    <ul>
                                        <li><i class="flaticon-maps-and-flags" aria-hidden="true"></i> Location</li>
                                        <li><i class="flaticon-phone-call"></i> (012)-345-6789</li>                                        
                                        <li><i class="flaticon-mail"></i> tourntravel@testmail.com</li>
                                    </ul>
                                </div>
                                <div class="footer-social-links">
                                    <ul>
                                        <li class="social-icon"><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                                        <li class="social-icon"><a href="#"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                                        <li class="social-icon"><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                                        <li class="social-icon"><a href="#"><i class="fa fa-youtube" aria-hidden="true"></i></a></li>
                                        <li class="social-icon"><a href="#"><i class="fa fa-google" aria-hidden="true"></i></a></li>
                                    </ul>
                                </div>    
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-4">
                            <div class="footer-links-list footer-margin">
                                <h3>Browse Tour</h3>
                                <ul>
                                    <li><a href="#">Cyclades <i class="fa fa-angle-right" aria-hidden="true"></i></a></li>
                                    <li><a href="#">North Ionian <i class="fa fa-angle-right" aria-hidden="true"></i></a></li>
                                    <li><a href="#">Sporades <i class="fa fa-angle-right" aria-hidden="true"></i></a></li>
                                    <li><a href="#">View all Cruises <i class="fa fa-angle-right" aria-hidden="true"></i></a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-4">
                            <div class="footer-recent-post footer-margin">
                                <h3>Recent Posts</h3>
                                <ul>
                                    <li>
                                        <div class="recent-post-item">
                                            <div class="recent-post-image">
                                                <img src="<?php echo base_url('assets/')?>images/bucket1.jpg" alt="Image">
                                            </div>
                                            <div class="recent-post-content">
                                                <h4><a href="#">A trip to heaven</a></h4>
                                                <p>June 17, 2019</p>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="recent-post-item">
                                            <div class="recent-post-image">
                                                <img src="<?php echo base_url('assets/')?>images/bucket2.jpg" alt="Image">
                                            </div>
                                            <div class="recent-post-content">
                                                <h4><a href="#">Diving in Atlantic</a></h4>
                                                <p>Jan 17, 2020</p>
                                            </div>
                                        </div>
                                    </li>
                                     
                                </ul>
                            </div>
                        </div>
                         
                    </div>
                </div>
            </div>
        </div>
        <div class="copyright">
            <div class="container">
                <div class="row">
                    <div class="col-xs-6">
                        <div class="copyright-content">
                            <p>2020 <i class="fa fa-copyright" aria-hidden="true"></i> wild & wide <a href="" target="_blank"></a></p>
                        </div>
                    </div>
                     
                </div>
            </div>
        </div>
    </footer>
    <!-- Footer Ends -->  

    <!-- Back to top start -->
    <div id="back-to-top">
        <a href="#"></a>
    </div>
    <!-- Back to top ends -->
    
    
</body>

</html>