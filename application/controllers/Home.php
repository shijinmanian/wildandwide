<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends MY_Controller {

    public function __construct() {
        parent::__construct();
    }

    public function index() {

        $this->load->view('header');
        $this->load->view('home');
        $this->load->view('footer');
    }

    public function enquiryAdd() {
        $data['status'] = 'error';

        $this->load->helper('security');
        $this->load->library('form_validation');

        $this->form_validation->set_rules('name', 'Name', 'trim|required');
        $this->form_validation->set_rules('email', 'Email', 'trim|required');
        $this->form_validation->set_rules('mobile', 'Phone no', 'trim');
        $this->form_validation->set_rules('destination', 'Destination', 'trim|required');

//        $this->form_validation->set_message('is_natural_no_zero', 'The %s is required');
        $this->form_validation->set_error_delimiters("<p>", "</p>");


        if ($this->form_validation->run() == TRUE) {
            $data['status'] = 'success';

            $formValues = [
                'name' => $this->input->post('name'),
                'email' => $this->input->post('email'),
                'mobile' => $this->input->post('mobile'),
                'destination' => $this->input->post('destination'),
                'dest_lat' => $this->input->post('destination_lat'),
                'dest_lng' => $this->input->post('destination_lng')
            ];
            $this->load->model("Enquiry_model", "Enquiry_model");

            $add = $this->Enquiry_model->add($formValues);
        } else {
            $data['message'] = validation_errors();
        }

        echo json_encode($data);
        exit;
    }

}
