<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Enquiry extends Admin_Controller {

    public function __construct() {
        parent::__construct();
    }

    public function index() {
        $this->load->model("Enquiry_model", "Enquiry_model");

        $data['enquiry'] = $this->Enquiry_model->getAll();

        $this->load->view('admin/header');
        $this->load->view('admin/enquiry', $data);
        $this->load->view('admin/footer');
    }

}
