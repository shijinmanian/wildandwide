<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Controller extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->helper('url');
    }

}

class Admin_Controller extends CI_Controller {

    function __construct() {
        parent::__construct();

        $this->load->library('ion_auth');
        if (!$this->ion_auth->logged_in()) {
            // redirect them to the login page
            redirect('/auth/login', 'refresh');
        }

        $this->load->helper(array('form', 'url'));
    }

}
