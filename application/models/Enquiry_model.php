<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Enquiry_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    public function add($data) {
        $data['created_at'] = date("Y-m-d H:i:s");
        //$data['created_by'] = $this->session->userdata('id');
 
        $this->db->insert('enquiry', $data);
        return $this->db->insert_id();
    }
    
    
    public function getAll(){
        $query = $this->db->get('enquiry');
        if ($query->num_rows() > 0) {
            return $query->result_array();
        }
        return array();
    }
}
